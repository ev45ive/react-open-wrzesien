export interface Album {
  id: string;
  name: string;
  images: AlbumImage[];
}

export interface AlbumImage {
  url: string;
  height?: number;
  width?: number;
}

export interface PagingObject<T> {
  items: T[];
  total?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
