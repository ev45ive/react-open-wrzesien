import { createStore, combineReducers } from "redux";
import { playlistsReducer, PlaylistsState } from "./reducers/playlists.reducer";

export type AppState = {
  playlists: PlaylistsState;
};

export const reducer = combineReducers({
  playlists: playlistsReducer
});

export const store = createStore(
  reducer,

  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);
