import { Reducer, Action, ActionCreator } from "redux";
import { Playlist } from "../models/Playlist";
import { createSelector } from "reselect";

export type PlaylistsState = {
  items: Playlist[];
  selectedId: Playlist["id"] | null;
};

const initialState = {
  items: [
    {
      id: 123,
      name: "React Hits!",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "React Top 20!",
      favorite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of React!",
      favorite: true,
      color: "#00ffff"
    }
  ],
  selectedId: 123
};

export const playlistsReducer: Reducer<PlaylistsState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "LOAD_PLAYLISTS":
      return {
        ...state,
        items: action.payload
      };

    case "SELECT_PLAYLIST":
      return { ...state, selectedId: action.payload };

    case "UPDATE_PLAYLIST": {
      const draft = action.payload;
      return {
        ...state,
        items: state.items.map(p => (p.id === draft.id ? draft : p))
      };
    }

    default:
      return state;
  }
};

type Actions = LOAD_PLAYLISTS | SELECT_PLAYLIST | UPDATE_PLAYLIST;

interface LOAD_PLAYLISTS extends Action<"LOAD_PLAYLISTS"> {
  payload: Playlist[];
}
interface SELECT_PLAYLIST extends Action<"SELECT_PLAYLIST"> {
  payload: Playlist["id"] | null;
}
interface UPDATE_PLAYLIST extends Action<"UPDATE_PLAYLIST"> {
  payload: Playlist;
}

export const playlistsLoad: ActionCreator<LOAD_PLAYLISTS> = (
  payload: Playlist[]
) => ({
  type: "LOAD_PLAYLISTS",
  payload
});

export const playlistSelect: ActionCreator<SELECT_PLAYLIST> = (
  payload: Playlist["id"] | null
) => ({
  type: "SELECT_PLAYLIST",
  payload
});

export const playlistUpdate: ActionCreator<UPDATE_PLAYLIST> = (
  payload: Playlist
) => ({
  type: "UPDATE_PLAYLIST",
  payload
});

export default playlistsReducer;

/* Selectors */

export const selectPlaylists = (state: { playlists: PlaylistsState }) =>
  state.playlists;

export const selectSelectedPlaylist = createSelector(
  selectPlaylists,
  state => state.items.find(p => p.id === state.selectedId) || null
);
