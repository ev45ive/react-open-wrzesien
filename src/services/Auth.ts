
export class AuthService {
  private token: string | null = null;

  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type = "token"
  ) {
    const rawToken = sessionStorage.getItem("token");
    if (rawToken) {
      this.token = JSON.parse(rawToken);
    }

    if (window.location.hash) {
      const match = window.location.hash.match(/#access_token=([^&]*)/);
      this.token = match && match[1];
      window.location.hash = "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }
  }

  authorize() {
    sessionStorage.removeItem("token");

    const url =
      `${this.auth_url}` +
      `?client_id=${this.client_id}` +
      `&redirect_uri=${this.redirect_uri}` +
      `&response_type=${this.response_type}`;

    window.location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
