import { AuthService } from "./Auth";
import { AlbumsResponse } from "../models/Album";
import axios, {  } from "axios";

export class MusicSearch {
  constructor(
    protected api_url: string,
    protected auth: AuthService // protected axios: AxiosInstance = axios
  ) {}

  search(q: string = "batman") {
    return axios
      .get<AlbumsResponse>(this.api_url, {
        params: {
          type: "album",
          query: q
        }
      })
      .then(res => res.data.albums.items);
    // .catch(err => Promise.reject(err.response.data.error.message));
  }
  // axios.post(this.api_url,{v:'placki'},{})

  // search(q: string = "batman") {
  //   return fetch(this.api_url + "?type=album&q=" + q, {
  //     headers: {
  //       Authorization: "Bearer " + this.auth.getToken()
  //     }
  //   })
  //     .then<AlbumsResponse>(res => {
  //       if (res.status >= 400) {
  //         return res.json().then(r => Promise.reject(r.error.message));
  //       }
  //       return res.json();
  //     })
  //     .then(resp => resp.albums.items);
  // }

  // sendData(){
  //   const dataToSend = {v:'placki'}

  //   return fetch(this.api_url + "?type=album&q=" + q, {
  //     headers: {
  //       ContentType: 'application/json',
  //       Authorization: "Bearer " + this.auth.getToken()
  //     },
  //     method:'POST',
  //     body: JSON.stringify(dataToSend)
  //   })
  // }
}
