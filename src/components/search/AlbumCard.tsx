import React from "react";
import { Album } from "../../models/Album";

type P = {
  album: Album;
} & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

export const AlbumCard: React.FC<P> = props => (
  <div {...props} className={"card " + props.className}>
    <img src={props.album.images[0].url} className="card-img-top" alt="" />
    <div className="card-body">
      <h5 className="card-title">{props.album.name}</h5>
    </div>
  </div>
);
