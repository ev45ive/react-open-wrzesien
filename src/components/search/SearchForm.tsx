import React, { useState, useContext, useEffect } from "react";
import { musicSearchContext } from "../../services";

type P = {
  onSearch(query: string): void;
};

export const SearchForm: React.FC<P> = ({ onSearch }) => {
  const [query, setQuery] = useState<string>("");

  // const search =  useContext(musicSearchContext).search

  useEffect(() => {
    if (!query) {
      return;
    }
    let handler = setTimeout(() => {
      onSearch(query);
    }, 400);

    return () => clearTimeout(handler);
  }, [query]);

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          onKeyUp={e => e.key === "Enter" && onSearch(query)}
          value={query}
          onChange={e => setQuery(e.target.value)}
        />
        <div className="input-group-append">
          <button className="btn btn-outline-secondary">Search</button>
        </div>
      </div>
    </div>
  );
};

// export class SearchForm extends React.PureComponent<P, S> {
//   state: S = { query: "" };

//   search = () => {
//     this.props.onSearch(this.state.query);
//   };

//   handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
//     this.setState({
//       query: event.target.value
//     });
//   };

//   handleKeyEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
//     if (event.key === "Enter") {
//       this.search();
//     }
//   };

//   timer!: NodeJS.Timer;

//   handleKeyDebounce = (event: React.KeyboardEvent<HTMLInputElement>) => {
//     // debounce
//     clearTimeout(this.timer);

//     this.timer = setTimeout(() => {
//       this.search();
//     }, 400);
//   };

//   render() {
//     console.log('render form')
//     return (
//       <div>
//         <div className="input-group mb-3">
//           <input
//             type="text"
//             className="form-control"
//             onKeyDown={this.handleKeyEnter}
//             onChange={this.handleInput}
//             value={this.state.query}
//             placeholder="Search"
//           />
//           <div className="input-group-append">
//             <button className="btn btn-outline-secondary" onClick={this.search}>
//               Search
//             </button>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
