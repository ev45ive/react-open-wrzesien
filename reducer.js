inc = (payload=1) => ({type:'INC', payload });
dec = (payload=1) => ({type:'DEC', payload });
todo = (payload='') => ({type:'ADD_TODO', payload });


const reducer = (state, action) => {
  switch(action.type){
      case 'INC': return {...state, counter: state.counter + action.payload }
      case 'DEC': return {...state, counter: state.counter - action.payload }
      case 'ADD_TODO': return {...state, todos: [...state.todos, action.payload] }
      default: return state;
  }
}

[inc(),inc(2),dec(),inc(1),todo('placki!')].reduce( reducer,{
	counter:0, todos:[]
})